package com.example.backend.service;

import com.example.backend.domain.Project;
import com.example.backend.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project addProject(Project project) throws ValidationException {
        String nameProject=project.getName();
        if(projectRepository.findByName(nameProject).isPresent())
        {
            throw new ValidationException("Project " + nameProject + " already exists!");
        }
        return projectRepository.save(project);
    }

    public List<Project> getAllProjects()
    {
        return projectRepository.findAll();
    }
}
