package com.example.backend.service;

import com.example.backend.domain.Project;
import com.example.backend.domain.TeamMember;
import com.example.backend.repository.ProjectRepository;
import com.example.backend.repository.TeamMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.Optional;

@Service
public class TeamMemberService {
    private final TeamMemberRepository teamMemberRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    public TeamMemberService(TeamMemberRepository teamMemberRepository, ProjectRepository projectRepository) {
        this.teamMemberRepository = teamMemberRepository;
        this.projectRepository = projectRepository;
    }

    public TeamMember addTeamMember(TeamMember teamMember) throws ValidationException {
        String email = teamMember.getEmail();
        if (teamMemberRepository.findByEmail(email).isPresent()) {
            throw new ValidationException("User " + email + " already exists");
        }
        return teamMemberRepository.save(teamMember);
    }

    public void linkTeamMemberWithProject(String email, String projectName) throws ValidationException {
        Optional<TeamMember> teamMemberOptional = teamMemberRepository.findByEmail(email);
        Optional<Project> projectOptional = projectRepository.findByName(projectName);
        if (teamMemberOptional.isEmpty()) {
            throw new ValidationException("User " + email + " doesn't exist");
        }
        if (projectOptional.isEmpty()) {
            throw new ValidationException("Project " + projectName + " doesn't exist");
        }
        if(teamMemberOptional.get().getProjects().stream().anyMatch(project -> project.getName().equals(projectName)))
        {
            throw new ValidationException("User has already been assigned to this project");
        }
        TeamMember teamMember=teamMemberOptional.get();
        teamMember.getProjects().add(projectOptional.get());
        teamMemberRepository.save(teamMember);
    }

    public List<TeamMember>getAllTeamMembers()
    {
        return teamMemberRepository.findAll();
    }
}
