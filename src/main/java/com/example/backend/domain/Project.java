package com.example.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String backgroundColor;
    private String fontColor;
    @ManyToMany(mappedBy = "projects")
    @JsonIgnore
    private List<TeamMember> teamMembers=new ArrayList<>();

    public Project(Long id, String name, String backgroundColor, String fontColor, List<TeamMember> teamMembers) {
        this.id=id;
        this.name = name;
        this.backgroundColor = backgroundColor;
        this.fontColor = fontColor;
        this.teamMembers = teamMembers;
    }
}
