package com.example.backend.domain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class TeamMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String name;
    private String role;
    @Column(columnDefinition = "TEXT")
    private String pictureUrl;
    @ManyToMany
    private List<Project> projects=new ArrayList<>();

    public TeamMember(Long id, String email, String name, String role, String pictureUrl, List<Project> projects) {
        this.id=id;
        this.email=email;
        this.name = name;
        this.role = role;
        this.pictureUrl = pictureUrl;
        this.projects = projects;
    }
}
