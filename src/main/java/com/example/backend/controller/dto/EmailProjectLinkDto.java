package com.example.backend.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailProjectLinkDto {
    private String email;
    private String projectName;
}
