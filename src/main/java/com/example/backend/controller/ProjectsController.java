package com.example.backend.controller;

import com.example.backend.domain.Project;
import com.example.backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectsController {
    private final ProjectService projectService;

    @Autowired
    public ProjectsController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/add")
    public ResponseEntity<String> addProject(@RequestBody Project project) {
        try {
            projectService.addProject(project);
            return ResponseEntity.ok("Project " + project.getName()+ " has been added");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<Project>>getAllProjects()
    {
        List<Project>projects=projectService.getAllProjects();
        return ResponseEntity.ok(projects);
    }
}
