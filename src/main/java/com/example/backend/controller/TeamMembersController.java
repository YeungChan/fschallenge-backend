package com.example.backend.controller;

import com.example.backend.controller.dto.EmailProjectLinkDto;
import com.example.backend.domain.TeamMember;
import com.example.backend.service.TeamMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teamMembers")
public class TeamMembersController {
    private final TeamMemberService teamMemberService;

    @Autowired
    public TeamMembersController(TeamMemberService teamMemberService) {
        this.teamMemberService = teamMemberService;
    }

    @PostMapping("/add")
    public ResponseEntity<String> addTeamMember(@RequestBody TeamMember teamMember) {
        try {
            teamMemberService.addTeamMember(teamMember);
            return ResponseEntity.ok().body(teamMember.getEmail() + " has been added");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/link")
    public ResponseEntity<String> linkTeamMemberWithProject(@RequestBody EmailProjectLinkDto teamMemberLinkProjectDto) {
        try {
            String email=teamMemberLinkProjectDto.getEmail();
            String projectName=teamMemberLinkProjectDto.getProjectName();
            teamMemberService.linkTeamMemberWithProject(teamMemberLinkProjectDto.getEmail(), teamMemberLinkProjectDto.getProjectName());
            return ResponseEntity.ok().body("Project: "+projectName+" is added to "+email);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<TeamMember>> getAllTeamMembers()
    {
        List<TeamMember> teamMembers=teamMemberService.getAllTeamMembers();
        return ResponseEntity.ok(teamMembers);
    }
}
